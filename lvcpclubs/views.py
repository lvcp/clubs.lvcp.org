from lvcpclubs import app, db
from flask import render_template

import datetime
from pytz import timezone

@app.route("/")
def club_list():
    clubs = db.get("clubs", {})
    clubkeys = sorted(filter(lambda club: clubs[club]["exists"], clubs))
    return render_template('club_list.html', clubs = clubs, clubkeys = clubkeys)
    
@app.route("/club/<clubslug>")
def club_detail(clubslug):
    return render_template('club_detail.html', club = db["clubs"][clubslug])
    
@app.route("/calendar")
def calendar():
    meetings = db.get("meetings",{})
    clubs = db.get("clubs",{})
    
    today = datetime.datetime.now(timezone(app.config["TIMEZONE"])).date()
    start_of_week = today
    
    if today.weekday() < 5: #Monday-Friday
        while start_of_week.weekday() > 0:
            start_of_week -= datetime.timedelta(days=1)
    else:
        while start_of_week.weekday() > 0:
            start_of_week += datetime.timedelta(days=1)
    
    weeks = [[start_of_week + datetime.timedelta(days=week*7 + weekday) for weekday in range(0,5)] for week in range(0,5)]
    
    return render_template('calendar.html', clubs = clubs, meetings = meetings, today = today, weeks = weeks,
        oneday = datetime.timedelta(days=1))