from lvcpclubs import app, manager, club_name_to_unique_key, db

import urllib, json, datetime
from pytz import utc, timezone
import strict_rfc3339
import os
import os.path

import gspread
from apiclient.discovery import build
from oauth2client.client import SignedJwtAssertionCredentials
from httplib2 import Http

client_email = app.config["G_CLIENT_EMAIL"]
with open("serviceaccount.p12") as f:
  private_key = f.read()

credentials = SignedJwtAssertionCredentials(client_email, private_key,
    ('https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/calendar.readonly'))
    
http_auth = credentials.authorize(Http())

#Arguments: data must be a dictionary that includes "name"
#is_authoritative_source_of is a list of properties that this data is the main/authoritative source of
#exists is a boolean. If False, the club might not actually exist (eg because the data is from the icon list)
def add_club_data(data, is_authoritative_source_of=[], exists=False):
    clubs = db.get("new_clubs",{})
    clubkey = club_name_to_unique_key(data["name"])
    club = clubs.get(clubkey,{"exists": False})
    for datakey in data:
        if not data[datakey]: continue
        if datakey not in club or datakey in is_authoritative_source_of:
            if datakey == "icon_url":
                downloaded_icons = db.get("downloaded_icons",{})
                if downloaded_icons.get(clubkey,"") != data[datakey]:
                    print "Downloading ",data[datakey], club.get("downloaded_icon_url")
                    if not os.path.exists("lvcpclubs/public/clubicons"):
                        os.makedirs("lvcpclubs/public/clubicons")
                    urllib.urlretrieve(data[datakey], "lvcpclubs/public/clubicons/"+clubkey+"."+data[datakey].split(".")[-1])
                club[datakey] = "/clubicons/"+clubkey+"."+data[datakey].split(".")[-1]
                downloaded_icons[clubkey] = data[datakey]
                db["downloaded_icons"] = downloaded_icons
            else:
                club[datakey] = data[datakey]
    if exists:
        club["exists"] = True
    clubs[clubkey] = club
    db["new_clubs"]=clubs
    
def add_club_meeting(name, date, time):
    clubkey = club_name_to_unique_key(name)
    meetings = db.get("new_meetings", {})
    meetings_on_date = meetings.get(date, {})
    meetings_at_time = meetings_on_date.get(time, [])
    if clubkey not in meetings_at_time:
        meetings_at_time.append(clubkey)
        meetings_on_date[time] = sorted(meetings_at_time)
        meetings[date] = meetings_on_date
        db["new_meetings"] = meetings

def update_blogger():
    tag_feed_text = http_auth.request("http://"+app.config['BLOG_DOMAIN']+"/feeds/posts/summary?alt=json&max-results=0","GET")[1]
    categories = sorted(map(lambda x: x["term"], json.loads(tag_feed_text)["feed"]["category"]))
    for category in categories:
        update_feed_text = http_auth.request("http://"+app.config['BLOG_DOMAIN']+"/feeds/posts/default/-/"+urllib.quote(category)+"?alt=json&max-results=20")[1]
        update_feed = json.loads(update_feed_text)
        updates = []
        for entry in update_feed["feed"]["entry"]:
            for potential_link in entry["link"]:
                if potential_link["rel"] == "alternate":
                    updates.append({"url": potential_link["href"], "title": potential_link["title"], "date": entry["published"]["$t"].split("T")[0]})
        add_club_data({"name": category, "updates": updates}, ["name", "updates"], True)
    return ""

def update_spreadsheet():
    gc = gspread.authorize(credentials)
    spreadsheet = gc.open_by_key(app.config["SPREADSHEET_ID"]).sheet1.get_all_values()
    
    #Figure out which columns of the spreadsheet correspond to which properties
    header = spreadsheet[0]
    columns = {}
    for column in range(len(header)):
        if header[column]:
            columns[header[column].lower()] = column
    
    #Get all the data 
    for row in spreadsheet[1:]:
        data_item = {}
        for column_name in columns:
            data_item[column_name] = row[columns[column_name]]
        if data_item["name"]:
            add_club_data(data_item, ["name", "icon_url", "Short_Description", "Long_Description", "Meeting_Day", "Meeting_Time", "Meeting_Location", "Student_Leader", "Student_Leader_Email"])
    return ""

def update_calendar():
    service = build('calendar', 'v3', http=http_auth)
    now = (datetime.datetime.utcnow() - datetime.timedelta(days=7) ).isoformat() + 'Z' # 'Z' indicates UTC time
    print('Getting the upcoming 1000 events')
    eventsResult = service.events().list(
        calendarId=app.config["CALENDAR_ID"], timeMin=now, maxResults=1000, singleEvents=True,
        orderBy='startTime').execute()
    events = eventsResult.get('items', [])

    if not events:
        print('No upcoming events found.')
    for event in events:
        start = event['start'].get('dateTime', event['start'].get('date'))
        start = datetime.datetime.fromtimestamp(strict_rfc3339.rfc3339_to_timestamp(start),utc)
        start = start.astimezone(timezone(app.config["TIMEZONE"]))
        startdate = start.date()
        starttimeobj = start.time()
        if starttimeobj < datetime.time(9,00):
            starttime = "before_school"
        elif starttimeobj < datetime.time(14,00):
            starttime = "lunch"
        else:
            starttime = "after_school"
        add_club_meeting(event['summary'], startdate, starttime)
        add_club_data({"name": event['summary'], "meeting_location": event.get('location', '')}, [], True)
    return ""

@manager.command
@app.route("/update_all/"+app.config["UPDATE_KEY"])
def update_all():
    db["new_clubs"] = {}
    db["new_meetings"] = {}
    print "Updating Blogger..."
    update_blogger()
    print "Updating spreadsheet..."
    update_spreadsheet()
    print "Updating calendar..."
    update_calendar()
    db["clubs"] = db["new_clubs"]
    db["meetings"] = db["new_meetings"]
    db["new_clubs"] = {}
    db["new_meetings"] = {}
    print "Done!"
    return "All club information has been updated."