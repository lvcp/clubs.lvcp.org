from flask import Flask, request
from flask.ext.script import Manager
from flaskext.zodb import ZODB

app = Flask(__name__, static_folder="public", static_url_path="")
app.config.from_object('lvcpclubs.config_default')
app.config.from_object('lvcpclubs.config')

db = ZODB(app)
manager = Manager(app)

#Turn a string that is probably a club name into a string that is probably a unique key
#Used to ensure different ways of writing a club's name are stored the same way in the database
#Examples:
#"Science Technology Engineering and Mathematics (STEM) Club" -> "stem"
#"Chinese Calligraphy Club" -> "chinese-calligraphy"
def club_name_to_unique_key(name):
    name = name.lower().split(" club")[0] # "Photo Club" -> "photo"

    name = name.split("/")[0] # "drama cabinet/its" -> "drama cabinet"

    name = name.replace("international thespian troupe", "drama")
    name = name.replace("esports", "e-sports")
    name = name.replace("esports", "e-sports")
    name = name.replace("travelling","traveling")
    
    name = name.replace("-"," ") # "sci-fi" -> "sci fi"
    name = name.replace("'","") # "rubik's" -> "rubiks"

    # Remove unimportant words from name ("the lvcp girls in tech club" -> "girls tech")
    words_in_name = name.split(" ")
    acronym_ignored_words = ["of", "in", "and", "an", "a", "the", "de"]
    ignored_words = acronym_ignored_words + ["lvcp", "organization", "club", "group", "america", "american"]
    
    if "drama" in words_in_name:
        ignored_words.append("cabinet") # "drama cabinet" -> "drama"
    
    for i in range(len(words_in_name)):
        if words_in_name[i].endswith("ing"):
            words_in_name[i] = words_in_name[i][:-3]
        words_in_name[i] = words_in_name[i].rstrip('s')
    
    words_without_ignored_words = []
    words_without_acronym_ignored_words = []
    for word in words_in_name:
        if word not in ignored_words not in ignored_words:
            if "(" not in word: # "model united nations (mun)" -> "model united nations"
                words_without_ignored_words.append(word)
        if word not in acronym_ignored_words and "(" not in word:
            words_without_acronym_ignored_words.append(word)
    
    if len(words_without_ignored_words) >= 3: # "future business leaders america" -> "fbla"
        name = ""
        for word in words_without_acronym_ignored_words:
            name += word[0]
    elif len(words_without_ignored_words) > 0: # Ensure "the america club" does not become ""
        name = "-".join(words_without_ignored_words) # "girl scouts"->"girl scout"
    elif len(words_without_acronym_ignored_words) > 0:
        name = "-".join(words_without_acronym_ignored_words)
    else:
        name = "-".join(words_in_name)
    
    return name

@app.after_request
def add_header(response):
    if "/update_all" not in request.path and response.status_code == 200:
        response.cache_control.max_age = 30
    return response

from lvcpclubs import updater, views
