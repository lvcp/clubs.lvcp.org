#Default config file. Make a copy called "config.py" and replace with your own values.

DEBUG = False
ZODB_STORAGE = 'file://app.fs'
G_CLIENT_EMAIL = '123456789-somethingsomething@developer.gserviceaccount.com'
BLOG_DOMAIN = 'lvcp-clubs.blogspot.com'
SPREADSHEET_ID = '15-8nNgoSp_pfBuMEP9VSJJDbbLoNEWJX3pTqNVIvc9c'
CALENDAR_ID = 'trivalleylearning.org_mhmpb144aerd198o7racs1mk0c@group.calendar.google.com'
TIMEZONE = 'US/Pacific'
UPDATE_KEY = 'TODO_SOMETHING_SECURE'