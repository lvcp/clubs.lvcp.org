[LVCP Clubs](http://clubs.lvcp.org/) is an online directory of clubs at Livermore Valley Charter Preparatory.

![Screenshot](screenshot.png)

It draws data from three sources: a [blog](http://lvcp-clubs.blogspot.com/), a [spreadsheet](https://docs.google.com/spreadsheets/d/15-8nNgoSp_pfBuMEP9VSJJDbbLoNEWJX3pTqNVIvc9c/edit?usp=sharing), and a [calendar](https://calendar.google.com/calendar/embed?src=trivalleylearning.org_mhmpb144aerd198o7racs1mk0c%40group.calendar.google.com&ctz=America/Los_Angeles).